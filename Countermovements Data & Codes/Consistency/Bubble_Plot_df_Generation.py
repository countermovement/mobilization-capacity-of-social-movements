import pandas as pd
import numpy as np
import re

Movement_Label = "BLM"

# Import the high active authors
High_Active_Authors = pd.read_pickle("/Countermovements Data & Codes/Consistency/"+Movement_Label+"_Authors.pkl")
High_Active_Authors = High_Active_Authors[High_Active_Authors["Label"]=="High"].reset_index()
High_Active_Authors["AuthorID"] = High_Active_Authors["AuthorID"].astype('int64')

# Import Hashtags
hashtag_df = pd.read_pickle("/Countermovements Data & Codes/Consistency/"+Movement_Label+"_Monthly_Top_hashtags.pkl")

def filter_topn_hashtags(df,topn):
    # Group result_df by 'Hashtag' and sum 'N_Tweets' within each group to get total usage count
    hashtag_totals = df.groupby('Hashtag')['N_Tweets'].sum().reset_index()

    # Sort the DataFrame by total usage count in descending order
    hashtag_totals_sorted = hashtag_totals.sort_values(by='N_Tweets', ascending=False)

    # Select the top n most used hashtags
    top_n_hashtags = hashtag_totals_sorted.head(topn)
    top_n_hashtags = top_n_hashtags['Hashtag'].tolist()

    return (df[df['Hashtag'].isin(top_n_hashtags)])


# Function to extract hashtags from a tweet
def extract_hashtags(tweet):
    hashtags = re.findall(r'#\w+', tweet)
    return hashtags

hashtag_df = filter_topn_hashtags(hashtag_df,50)
hashtag_list = hashtag_df["Hashtag"].unique()


total_volumes=[]
unique_user_counts=[]
high_active_fractions=[]


# Initialize an empty list to store filtered chunks
i=0

for hashtag in hashtag_list:
    filtered_df = pd.read_pickle("/Countermovements Data & Codes/Data and Misc/Data_After_Spam_Detection/"+Movement_Label+"_df.pkl") 
    filtered_df["Tweet"] = filtered_df["Tweet"].str.lower()
    filtered_df['hashtags'] = filtered_df['Tweet'].apply(extract_hashtags)
    filtered_df = filtered_df[filtered_df['hashtags'].apply(lambda x: hashtag in x)]
    filtered_df["AuthorID"] =filtered_df["AuthorID"].astype("int64")

    # Store the total volume, number of unique users, and proportion of tweets posted by high active users
    total_volume= len(filtered_df)
    total_volumes.append(total_volume)

    unique_user_count = len(filtered_df["AuthorID"].unique())
    unique_user_counts.append(unique_user_count)

    high_active_tweet_volume = len(filtered_df[filtered_df["AuthorID"].isin(High_Active_Authors['AuthorID'])])
    if total_volume == 0:
        high_active_fractions.append(0)
    else:
        high_active_fractions.append(high_active_tweet_volume/total_volume)
    i += 1
    print(i)

Bubble_plot_df = pd.DataFrame(data = {"Hashtag" : hashtag_list,"percentage": high_active_fractions,"Total": total_volumes, "Nof_Users":unique_user_counts})

def calculate_consistency_score(results_df, threshold):
    # Calculate consistency score for each hashtag in each month
    consistency_scores = []

    # Iterate over each month
    for month in results_df['Month'].unique():
        # Filter the DataFrame for the current month
        month_df = results_df[results_df['Month'] == month]

        # Iterate over each row in the DataFrame for the current month
        for index, row in month_df.iterrows():
            hashtag = row['Hashtag']
            n_tweets = row['N_Tweets']

            # Calculate consistency score based on threshold
            score = 1 if n_tweets > threshold else 0

            # Append the consistency score to the list
            consistency_scores.append({'Hashtag': hashtag, 'Consistency_Score': score})

    # Convert the list of consistency scores to a DataFrame
    consistency_scores_df = pd.DataFrame(consistency_scores)

    return consistency_scores_df.groupby("Hashtag").mean()

df_Consistencies = calculate_consistency_score(hashtag_df,50)

Bubble_plot_df=Bubble_plot_df.merge(df_Consistencies,on="Hashtag",how="left")

Bubble_plot_df.to_csv("/Countermovements Data & Codes/Consistency/"+Movement_Label+"_Bubble.csv")