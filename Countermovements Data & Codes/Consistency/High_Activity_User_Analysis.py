import pandas as pd
import numpy as np


# Load the data from a pickle file
df = pd.read_pickle('/Countermovements Data & Codes/Data and Misc/Data_After_Spam_Detection/BLM_df.pkl')
df = pd.read_pickle("/Countermovements Data & Codes/Data and Misc/Data_After_Spam_Detection/ALM_df.pkl")
df = pd.read_pickle("/Countermovements Data & Codes/Data and Misc/Data_After_Spam_Detection/P-BLM_df.pkl")
df = pd.read_pickle("/Countermovements Data & Codes/Data and Misc/Data_After_Spam_Detection/WLM_df.pkl")
len(df)

# Group by AuthorID and count the number of tweets
author_tweet_count = df.groupby('AuthorID')['Tweet'].count().reset_index()

# Rename the column 'Tweet' to 'N_Tweets'
author_tweet_count = author_tweet_count.rename(columns={'Tweet': 'N_Tweets'})

# Find quantiles for the number of posted total tweets
quantiles = author_tweet_count['N_Tweets'].quantile([0.90, 0.99, 1.0])

# Function to label authors
def label_authors(row):
    if row['N_Tweets'] >= quantiles[0.99]:
        return 'High'
    elif row['N_Tweets'] >= quantiles[0.90]:
        return 'Medium'
    else:
        return 'Low'

# Add author labels
author_tweet_count['Label'] = author_tweet_count.apply(label_authors, axis=1)

# Save the resulting table
author_tweet_count.to_pickle("/Countermovements Data & Codes/Consistency/WLM_Authors.pkl")
# author_tweet_count.to_pickle("/Countermovements Data & Codes/Consistency/ALM_Authors.pkl")
# author_tweet_count.to_pickle("/Countermovements Data & Codes/Consistency/P-BLM_Authors.pkl")
# author_tweet_count.to_pickle("/Countermovements Data & Codes/Consistency/BLM_Authors.pkl")

