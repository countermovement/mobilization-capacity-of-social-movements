import os
import re
import pandas as pd
import preprocessor as p


# Prepare data

input_path = "/Countermovements Data & Codes/Data and Misc/2yearsBlackLivesMatter_en.json"
df_json = pd.read_json(input_path)


df = pd.DataFrame()
df["Tweet"] = df_json["text"]
df["TweetID"] =df_json["id"]
df["Time"] =df_json["created_at"]
def preprocess_activity_count(row):
    activities_df = pd.DataFrame(row["public_metrics"].items(), columns=['Activity', 'Count'])
    return activities_df["Count"]

activities_df=df_json.apply(preprocess_activity_count, axis=1)
activities_df.columns={"Retweet_Count", "Reply_Count", "Like_Count", "Quote_Count"}
df = pd.concat([df, activities_df], axis=1)
def preprocess_author_id(row):
    return row["author"]["id"]
df["AuthorID"]=df_json.apply(preprocess_author_id, axis=1)

output_path = "/Countermovements Data & Codes/Data and Misc/2yearsBlackLivesMatter_en.pkl"
df.to_pickle(output_path)

